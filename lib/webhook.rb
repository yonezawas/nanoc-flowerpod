# encoding: utf-8

require 'httparty'
require 'nokogiri'
require 'systemu'

class Webhook < Nanoc::Extra::Deployer
  identifier :webhook

  def run
    modifeid_dates = get_created_at_items
    return unless modifeid_dates.size == 1 # ignores two changes...
    updated = modifeid_dates.first
    href = find_entry_by_updated(updated)
    return if href.nil? # do nothing...
    send_webhook_to_discord(href)
  end

  private

    def send_webhook_to_discord(content)
      webhook_url = ENV['WEBHOOK_URL']
      if webhook_url
        HTTParty.post(webhook_url,
        multipart: true,
        body: {
          content: content
        })
        puts "sent webhook to discord: #{content}"
      end
    end

    def find_entry_by_updated(updated)
      filename = config.fetch(:filename, 'feed.xml')
      feed_path = File.expand_path(filename, source_path)
      raise 'feed_path does not exists in output' unless File.exists?(feed_path)
      feed_doc = File.open(feed_path) { |f| Nokogiri::XML(f) }
      entry = feed_doc.css('entry').find { |e| e.css('updated').text === updated }
      entry.instance_of?(Nokogiri::XML::Element) && entry.css('link').attribute('href').value
    end

    def get_created_at_items
      items = get_modified_files.map do |target_file|
        content = File.read(target_file, :encoding => 'UTF-8')
        _, metadata = content.split("---\n")
        next if metadata.nil?
        attrs = YAML.load(metadata)
        next if attrs['kind'] != 'article'
        attrs['created_at'].__nanoc_to_iso8601_time
      end
      items.compact
    end

    def get_modified_files
      arg = ["git", "diff-tree", "--no-commit-id", "--name-only", "-r", "HEAD"]
      stdout = ''
      status = systemu(arg, 'stdout' => stdout)
      raise "getting name status failed" unless status.success?
      stdout.each_line.map do |line|
        line.chomp
      end
    end
end
